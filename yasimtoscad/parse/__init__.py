import numpy as np
from dataclasses import dataclass
import enum

KG_PER_LB = 0.453592


class RotationalDirection(enum.Enum):
    clockwise = enum.auto()
    counterclockwise = enum.auto()


def _get_mass_entry_in_kg(item, entry_type):
    #  mass types defaults to lbs
    if item.attrib.get(f"{entry_type}"):
        mass_kg = float(item.attrib.get(f"{entry_type}")) * KG_PER_LB
    elif item.attrib.get(f"{entry_type}-lbs"):
        mass_kg = float(item.attrib.get(f"{entry_type}-lbs")) * KG_PER_LB
    elif item.attrib.get(f"{entry_type}-kg"):
        mass_kg = float(item.attrib.get(f"{entry_type}-kg"))
    else:
        raise(SystemError)
    return mass_kg


@dataclass(frozen=True)
class FuselageClass:
    start:    np.array
    end:      np.array
    midpoint: float
    width:    float
    taper:    float


def fuselage(item):

    # the start of the fuselage component
    start = np.array([
        float(item.attrib.get('ax')),
        float(item.attrib.get("ay")),
        float(item.attrib.get("az"))])

    # the end of the fuselate component
    end = np.array([
        float(item.attrib.get('bx')),
        float(item.attrib.get("by")),
        float(item.attrib.get("bz"))])

    # the midpoint of the fuselage as a ratio to the length
    midpoint = float(item.attrib.get("midpoint", 1))
    # the cylinder diameter at the mid point
    width = float(item.attrib.get("width"))
    # t is the end of the fuselate component
    taper = float(item.attrib.get("taper", 1))

    fuselage_data = FuselageClass(
        start=start,
        end=end,
        midpoint=midpoint,
        width=width,
        taper=taper)
    return fuselage_data


@dataclass(frozen=True)
class WingElementClass:
    wing_element_type: str
    start:             float
    end:               float


@dataclass(frozen=True)
class WingClass:
    type:      str
    root:      np.array
    mirrored:  bool
    dihedral:  float
    chord:     float
    length:    float
    taper:     float = 1  # defaults to 1
    sweep:     float = 0  # defaults to 0
    incidence: float = 0  # defaults to 0
    wing_elements: any = None


def wing(item):

    # set mirrored = True if handling wing mstab or hstab, else False

    mirrored = item.tag in {"wing", "mstab", "hstab"}

    if item.tag in {"vstab"}:
        dihedral = float(item.attrib.get("dihedral",  90))  # defaults to 90
    else:
        dihedral = float(item.attrib.get("dihedral",  0))  # defaults to 0

    root = np.array([
        float(item.attrib.get('x')),
        float(item.attrib.get("y")),
        float(item.attrib.get("z"))])

    incidence = float(item.attrib.get("incidence", 0))  # defaults to 0
    chord = float(item.attrib.get("chord"))
    length = float(item.attrib.get("length"))
    taper = float(item.attrib.get("taper",     1))  # defaults to 1
    sweep = float(item.attrib.get("sweep",     0))  # defaults to 0
    # camber is not needed for visualisation

    wing_data = WingClass(
        type=item.tag,
        root=root,
        mirrored=mirrored,
        dihedral=dihedral,
        chord=chord,
        length=length,
        taper=taper,
        sweep=sweep,
        incidence=incidence,
        wing_elements=[]
    )

    for subitem in item:
        if subitem.tag in {"flap0", "flap1", "spoiler", "slat"}:
            wing_element_type = subitem.tag
            start = float(subitem.attrib["start"])
            end = float(subitem.attrib["end"])
            wing_element = WingElementClass(
                wing_element_type=wing_element_type,
                start=start,
                end=end)
            wing_data.wing_elements.append(wing_element)

    return wing_data


@dataclass(frozen=True)
class PropellerClass:
    origin:       np.array
    radius:       float
    direction:    RotationalDirection
    action_point: np.array = None


def propeller(item):
    root = np.array([
            float(item.attrib.get("x")),
            float(item.attrib.get("y")),
            float(item.attrib.get("z")),
            ])

    action_point = None
    for subitem in item:
        if subitem.tag in {'actionpt'}:
            action_point = np.array([
                    float(subitem.attrib.get("x")),
                    float(subitem.attrib.get("y")),
                    float(subitem.attrib.get("z")),
                    ])

    radius = float(item.attrib.get("radius"))
    moment = float(item.attrib.get("moment"))

    # direction of rotation is derived from object's mass
    if moment > 0:
        direction = RotationalDirection.clockwise
    else:
        direction = RotationalDirection.counterclockwise

    propeller_data = PropellerClass(
        origin=root,
        radius=radius,
        action_point=action_point,
        direction=direction)
    return propeller_data


@dataclass(frozen=True)
class TankClass:
    origin:      np.array
    capacity_kg: float
    jet:         bool


def tank(item):
    origin = np.array([
            float(item.attrib.get("x")),
            float(item.attrib.get("y")),
            float(item.attrib.get("z")),
            ])

    capacity_kg = _get_mass_entry_in_kg(item, "capacity")

    # tank size is based on weight and density, choose the density based on
    # fuel type
    jet = bool(item.attrib.get("jet", False))

    tank_data = TankClass(origin=origin, capacity_kg=capacity_kg, jet=jet)
    return tank_data


@dataclass(frozen=True)
class WeightClass:
    origin: np.array
    size:   float


def weight(item):
    origin = np.array([
            float(item.attrib.get("x")),
            float(item.attrib.get("y")),
            float(item.attrib.get("z")),
            ])
    size = item.attrib.get("size")
    if size:
        size = float(size)
    else:
        # if no size given use .3 metre diameter for visualisation
        size = .3

    weight_data = WeightClass(origin=origin, size=size)
    return weight_data


@dataclass(frozen=True)
class BallastClass:
    origin:  np.array
    mass_kg: float


def ballast(item):
    origin = np.array([
            float(item.attrib.get("x")),
            float(item.attrib.get("y")),
            float(item.attrib.get("y")),
           ])
    # mass can be negative, so to picture it make it positive
    mass_kg = abs(_get_mass_entry_in_kg(item, "mass"))

    ballast_data = BallastClass(origin=origin, mass_kg=mass_kg)
    return ballast_data


@dataclass(frozen=True)
class RotorClass:
    origin:   np.array
    normal:   np.array
    diameter: float
    direction: RotationalDirection


def rotor(item):
    origin = np.array([
            float(item.attrib.get("x")),
            float(item.attrib.get("y")),
            float(item.attrib.get("z")),
            ])

    rotor_normal = np.array([
            float(item.attrib.get("nx")),
            float(item.attrib.get("ny")),
            float(item.attrib.get("nz")),
            ])

    diameter = float(item.attrib.get("diameter"))

    direction_entry = item.attrib.get("ccw")

    if direction_entry in {"1", "true"}:
        direction = RotationalDirection.counterclockwise
    elif direction_entry in {"0", "false"}:
        direction = RotationalDirection.clockwise

    rotor_data = RotorClass(
        origin=origin,
        normal=rotor_normal,
        diameter=diameter,
        direction=direction)
    return rotor_data


@dataclass(frozen=True)
class ThrusterClass:
    origin:    np.array
    vector:    np.array
    thrust_kg: float


def thruster(item):
    origin = np.array([
            float(item.attrib.get("x")),
            float(item.attrib.get("y")),
            float(item.attrib.get("z")),
            ])

    vector = np.array([
            float(item.attrib.get("vx")),
            float(item.attrib.get("vy")),
            float(item.attrib.get("vz")),
            ])

    thrust_kg = _get_mass_entry_in_kg(item, "thrust")

    thruster_data = ThrusterClass(
        origin=origin,
        vector=vector,
        thrust_kg=thrust_kg)
    return thruster_data


@dataclass(frozen=True)
class JetClass:
    origin:    np.array
    mass_kg:   float
    thrust_kg: float
    rotate:    float
    action_point: np.array


def jet(item):
    origin = np.array([
            float(item.attrib.get("x")),
            float(item.attrib.get("y")),
            float(item.attrib.get("z")),
            ])
    mass_kg = _get_mass_entry_in_kg(item, "mass")

    thrust_kg = _get_mass_entry_in_kg(item, "thrust")

    rotate = float(item.attrib.get("rotate", 0.))

    action_point = None
    for subitem in item:
        if subitem.tag in {'actionpt'}:
            action_point = np.array([
                    float(subitem.attrib.get("x")),
                    float(subitem.attrib.get("y")),
                    float(subitem.attrib.get("z")),
                    ])

    jet_data = JetClass(
        origin=origin,
        mass_kg=mass_kg,
        thrust_kg=thrust_kg,
        rotate=rotate,
        action_point=action_point)
    return jet_data


@dataclass(frozen=True)
class GearClass:
    origin: np.array


def gear(item):
    origin = np.array([
            float(item.attrib.get("x")),
            float(item.attrib.get("y")),
            float(item.attrib.get("z")),
            ])

    gear_data = GearClass(origin=origin)
    return gear_data


@dataclass(frozen=True)
class CockpitClass:
    origin: np.array


def cockpit(item):
    origin = np.array([
            float(item.attrib.get("x")),
            float(item.attrib.get("y")),
            float(item.attrib.get("z")),
            ])

    cockpit_data = CockpitClass(origin=origin)
    return cockpit_data

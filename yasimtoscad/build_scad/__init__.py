import yasimtoscad
import yasimtoscad.parse

import math
import numpy as np

import solid as scad

DEG2RAD = math.pi / 180
KG_PER_LB = 0.453592
LITRESTOCUBICM = 1000  # Litres per cubic metre

# control surfaces as ratio of wing chord
SLATRATIO = .2
FLAPRATIO = .2
SPOILERRATIO = .2


def _build_volume(volume, length_to_girth):

    # volume is in m^3

    # https://www.vcalc.com/wiki/vCalc/Ellipsoid+-+Volume
    # volume of ellipsoid = (4 / 3) * pi * a * b * c
    # make our tank length_to_girth times longer than its girth
    # a = measurement from centre to surface along x
    # b = c
    # v = (4/3) * pi * (length_to_girth * b) * b * b
    # v / (4/3) = pi * (length_to_girth * b) * b * b
    # (v / (4/3)) / pi = length_to_girth * b ^ 3
    # b ^ 3 = ((v / (4/3)) / pi) / length_to_girth
    # b =  (((v / (4/3)) / pi) / length_to_girth) ^ (1/3)

    r = (((volume / (4./3.)) / math.pi) / length_to_girth) ** (1/3)

    volume_obj = scad.sphere(r=r, segments=36)
    # make our tank length_to_girth times longer than girth
    volume_obj = scad.scale([length_to_girth, 1, 1])(volume_obj)
    return volume_obj


def _get_vector_transform_from_z(target):
    mm = _get_vector_transform_from_source(
        source=np.array([0, 0, 1]),
        target=target)
    return(mm)


def _get_vector_transform_from_source(source, target):
    # http://forum.openscad.org/Rods-between-3D-points-td13104.html

    # h = the overall length of the fuselage
    h = np.linalg.norm(target)

    # w is the unit vector in the new z-axis
    w = target / h
    # calculate the direction of the  new z-axis (u0) by taking the
    # cross product of the target orientation and the original z-axis
    u0 = np.cross(w, source)

    # is x and y of the target direction is 0 then the fuselage is
    # aligned along
    # the z-axis and we can use it as if not, fix orientation
    # u is the new x-axis as a unit vector
    if (target[0] == target[1] == 0):
        # if the x and y components of target are 0 use unit x as u
        u = np.array([1.0, 0.0, 0.0])
    else:
        u = u0 / np.linalg.norm(u0)
    # calculate the direction of the new y-axis (v0) by taking the
    # cross product of the target orientation and the new x-axis
    v0 = np.cross(w, u)

    # v is the new y-axis as a unit vector
    if (target[0] == target[1] == 0):
        # if the x and y components of target are 0 use unit y as v
        v = np.array([0.0, 1.0, 0.0])
    else:
        v = v0 / np.linalg.norm(v0)

    # apply transformation with multimatrix
    # u is new x, v is new y, w is new z
    m = [[u[0], v[0], w[0], 0],
         [u[1], v[1], w[1], 0],
         [u[2], v[2], w[2], 0],
         [0,       0,    0, 1]]
    return m


def fuselage(fuselage_data):

    # target is the target direction and length of the fuselage as a vector
    # from the origin
    target = fuselage_data.end - fuselage_data.start

    # h = the overall length of the fuselage
    length = np.linalg.norm(target)

    length_to_midpoint = length * fuselage_data.midpoint

    length_from_midpoint = length * (1 - fuselage_data.midpoint)

    diameter_at_midpoint = fuselage_data.width
    diameter_at_end = fuselage_data.width * fuselage_data.taper

    # Create fuselage shape from two cylinders using YASim taper
    # OpenSCAD creates cylinders in the +z direction
    # fuse_a is the first part of the fuselafe from origin to mid point
    fuse_a = scad.cylinder(
        h=length_to_midpoint,
        d1=diameter_at_end,
        d2=diameter_at_midpoint,
        segments=36)
    # fuse_b is the second part of the fuselafe from mid point to end
    fuse_b = scad.cylinder(
        h=length_from_midpoint,
        d1=diameter_at_midpoint,
        d2=diameter_at_end,
        segments=36)
    # fuse is fuse_b added to the end of fuse_a
    fuse = fuse_a + scad.translate([0, 0, length_to_midpoint])(fuse_b)

    # get the multimatrix to align fuselage along the start to end points
    mmatrix = _get_vector_transform_from_z(target)

    # apply transformation with multimatrix
    fuse = scad.multmatrix(m=mmatrix)(fuse)
    # move fuselage to start point
    fuse = scad.translate(fuselage_data.start)(fuse)

    # assign colour with transparency
    fuse = scad.color(**yasimtoscad.item_color['fuselage'])(fuse)

    fuse.set_modifier('background')
    return fuse


def rotor(rotor_data):
    origin = rotor_data.origin
    vector = rotor_data.normal
    direction = rotor_data.direction
    diameter = rotor_data.diameter
    height = rotor_data.diameter * 0.01

    direction_arrow = scad.cylinder(
        d1=height*5,
        d2=0,
        h=height * 9,
        segments=36,
        center=True)
    # position the direction arrow at the leading edge of the rotor disc
    # YASim considers rotational direction viewed from above for rotors
    if direction == yasimtoscad.parse.RotationalDirection.counterclockwise:
        arrow_rotation = [-90, 0, 0]  # point left
    else:
        arrow_rotation = [90, 0, 0]  # point to the right
    direction_arrow = scad.rotate(arrow_rotation)(direction_arrow)
    # move the rotation arrow to leading edge
    direction_arrow = scad.translate([diameter/2, 0, 0])(direction_arrow)

    rotor_mesh = scad.cylinder(
        d=rotor_data.diameter,
        h=height,
        segments=36,
        center=True)
    rotor_mesh += direction_arrow
    mmatrix = _get_vector_transform_from_z(vector)

    rotor_mesh = scad.multmatrix(m=mmatrix)(rotor_mesh)
    rotor_mesh = scad.translate(origin)(rotor_mesh)
    rotor_mesh = scad.color(**yasimtoscad.item_color['rotor'])(rotor_mesh)
    return rotor_mesh


def thruster(thruster_data):

    origin = thruster_data.origin
    vector = thruster_data.vector

    mv = _get_vector_transform_from_z(vector)

    # add offset of origin to mmatrix
    mv[0][3] += origin[0]
    mv[1][3] += origin[1]
    mv[2][3] += origin[2]

    # Use the thrust to size the jet exhaust diameter with fiddle factor
    jet_diameter = thruster_data.thrust_kg / 5000
    # Use the thrust to size the jet exhaust length with fiddle factor
    exhaust_length = thruster_data.thrust_kg / 200

    thruster_mesh = scad.cylinder(
        d1=jet_diameter, d2=0,
        h=exhaust_length, segments=36)
    thruster_mesh = scad.multmatrix(m=mv)(thruster_mesh)
    thruster_mesh = scad.color(
            **yasimtoscad.item_color['thruster']
        )(thruster_mesh)
    return thruster_mesh


def cockpit(cockpit_data):

    origin = cockpit_data.origin
    radius = 0.15  # Choose arbitrary size for head based on helmet

    cockpit_mesh = scad.sphere(r=radius, segments=36)
    cockpit_mesh = scad.translate(origin)(cockpit_mesh)
    cockpit_mesh = scad.color(
            **yasimtoscad.item_color['cockpit']
        )(cockpit_mesh)
    return cockpit_mesh


def jet(jet_data):

    jet_origin = jet_data.origin
    jet_mass = jet_data.mass_kg
    jet_thrust = jet_data.thrust_kg
    jet_rotate = jet_data.rotate
    action_point = jet_data.action_point

    # use Specifications (T56 Series IV) to get a baseline for mass/volume
    # https://en.wikipedia.org/wiki/Allison_T56

    turbine_mass = 880  # kg
    turbine_radius = 0.69 / 2  # metres
    turbine_length = 3.71      # metres
    turbine_length_to_radius = turbine_length / turbine_radius

    # m^3 v = pi * r^2 * l
    turbine_volume = math.pi * (turbine_radius ** 2) * turbine_length
    # kg/m^3
    turbine_density = turbine_mass / turbine_volume

    # m^3
    jet_volume = jet_mass / turbine_density

    # turbine_volume = math.pi * turbine_radius ** 2 * turbine_length
    # turbine_volume =
    #     math.pi * turbine_radius ** 2
    #     * turbine_radius * turbine_length_to_mass
    # turbine_volume = math.pi * turbine_radius ** 3 * turbine_length_to_mass
    # jet_volume = math.pi * jet_radius ** 3 * turbine_length_to_mass
    # jet_volume /(math.pi * turbine_length_to_mass) = jet_radius ** 3
    jet_radius = (jet_volume / (math.pi * turbine_length_to_radius)) ** (1/3)
    jet_length = jet_radius * turbine_length_to_radius

    # Use the thrust to size the jet exhaust length with fiddle factor
    exhaust_length = jet_thrust * .003

    jet_mesh = scad.cylinder(
        r=jet_radius, h=jet_length,
        segments=36, center=True)
    jet_mesh = scad.rotate([0, -90, 0])(jet_mesh)
    jet_mesh = scad.translate(jet_origin)(jet_mesh)
    jet_mesh = scad.color(**yasimtoscad.item_color['jet'])(jet_mesh)

    if action_point is None:
        # Set jet exhaust to align with end of engine
        action_point = jet_origin + [-jet_length/2, 0, 0]

    exhaust_mesh = scad.cylinder(
        r1=jet_radius, r2=0,
        h=exhaust_length, segments=36)
    exhaust_mesh = scad.rotate([0, -90, 0])(exhaust_mesh)
    exhaust_mesh = scad.translate(action_point)(exhaust_mesh)
    exhaust_mesh = scad.color(
            **yasimtoscad.item_color['exhaust']
        )(exhaust_mesh)

    if jet_rotate is not None and not jet_rotate == 0:
        rotated_exhaust_mesh = scad.cylinder(
            r1=jet_radius, r2=0,
            h=exhaust_length, segments=36)
        rotated_exhaust_mesh = scad.rotate(
                [0, -90 + jet_rotate, 0]
            )(rotated_exhaust_mesh)
        rotated_exhaust_mesh = scad.translate(
                action_point
            )(rotated_exhaust_mesh)
        rotated_exhaust_mesh = scad.color(
                **yasimtoscad.item_color['rotated_exhaust']
            )(rotated_exhaust_mesh)
        return_mesh = jet_mesh + exhaust_mesh + rotated_exhaust_mesh
    else:
        return_mesh = jet_mesh + exhaust_mesh

    return return_mesh


def gear(gear_data):

    wheel_radius = 0.1  # Choose arbitrary size for wheel
    # root marks the contact point adjust up for the wheel radius
    gear_data.origin[2] = gear_data.origin[2] + wheel_radius
    wheel_width = wheel_radius*0.2

    gear_mesh = scad.cylinder(
        r=wheel_radius, h=wheel_width,
        segments=36, center=True)
    gear_mesh = scad.rotate([90, 0, 0])(gear_mesh)
    gear_mesh = scad.translate(gear_data.origin)(gear_mesh)
    gear_mesh = scad.color(**yasimtoscad.item_color['gear'])(gear_mesh)
    return gear_mesh


def weight(weight_data):

    weight_mesh = scad.sphere(d=weight_data.size, segments=36)
    weight_mesh = scad.translate(weight_data.origin)(weight_mesh)
    weight_mesh = scad.color(**yasimtoscad.item_color['weight'])(weight_mesh)
    return weight_mesh


def ballast(ballast_data):

    # for visualisation purposes use volume of water to depict ballast
    density = 1  # kg/l
    volume = ballast_data.mass_kg * density / 1000  # in m^3

    length_to_girth = 4  # make ballast 4 times longer than its girth
    ballast_mesh = _build_volume(volume, length_to_girth)
    ballast_mesh = scad.translate(ballast_data.origin)(ballast_mesh)
    ballast_mesh = scad.color(
            **yasimtoscad.item_color['ballast']
        )(ballast_mesh)
    return ballast_mesh


def tank(tank_data):

    if tank_data.jet:
        # density of jet fuel
        density = 0.840  # kg/l (750-780 g/l)
    else:
        # density of gasoline
        density = 0.780  # kg/l (715-780 g/l)

    volume = ((tank_data.capacity_kg) / density) / LITRESTOCUBICM  # in m^3

    length_to_girth = 4

    tank_mesh = _build_volume(volume, length_to_girth)
    tank_mesh = scad.translate(tank_data.origin)(tank_mesh)
    tank_mesh = scad.color(**yasimtoscad.item_color['tank'])(tank_mesh)
    return tank_mesh


def propeller(propeller_data):

    radius = propeller_data.radius
    thickness = propeller_data.radius*0.05
    origin = propeller_data.origin
    action_point = propeller_data.action_point
    direction = propeller_data.direction

    direction_arrow = scad.cylinder(
        d1=thickness*5,
        d2=0,
        h=thickness * 9,
        segments=36,
        center=True)
    # position the direction arrow at the upper edge of the rotor disc
    # YASim considers rotational direction viewed from behind for propellers
    if direction == yasimtoscad.parse.RotationalDirection.counterclockwise:
        arrow_rotation = [-90, 0, 0]  # point left
    else:
        arrow_rotation = [90, 0, 0]  # point to the right
    direction_arrow = scad.rotate(arrow_rotation)(direction_arrow)
    # move the rotation arrow to upper edge
    direction_arrow = scad.translate([-radius, 0, 0])(direction_arrow)

    propeller_mesh = scad.cylinder(r=radius, h=thickness, segments=36)
    propeller_mesh += direction_arrow
    propeller_mesh = scad.rotate([0, 90, 0])(propeller_mesh)
    if action_point is None:
        propeller_mesh = scad.translate(origin)(propeller_mesh)
    else:
        propeller_mesh = scad.translate(action_point)(propeller_mesh)
    propeller_mesh = scad.color(
            **yasimtoscad.item_color['propeller']
        )(propeller_mesh)
    return propeller_mesh


def wing(wing_data):

    WINGTHICKNESSRATIO = .1
    SPOILERTHICKNESSRATIO = .11
    wing_object = scad.union()

    wing_type = wing_data.type

    wing_taper = wing_data.taper
    wing_length = wing_data.length
    root_chord = wing_data.chord

    tip_chord = root_chord * wing_taper

    wing_base = scad.cylinder(
        d1=root_chord, d2=tip_chord,
        h=wing_length, segments=36)

    # approximate the aerofoil shape by squashing the circle into an ellipse

    aerofoil_base = scad.scale([1, WINGTHICKNESSRATIO, 1])(wing_base.copy())
    flap_base = scad.scale([1.02, WINGTHICKNESSRATIO, 1])(wing_base.copy())
    slat_base = scad.scale([1.02, WINGTHICKNESSRATIO, 1])(wing_base.copy())
    spoiler_base = scad.scale([1, SPOILERTHICKNESSRATIO, 1])(wing_base.copy())

    # calculate the angle due to the taper of the wing to enable matching of
    # the masks to the leading and trailing edges of the wings accounting for
    # the taper
    # alpha = math.acos(self.taper)
    adjacent = wing_length
    opposite = (root_chord - tip_chord) / 2
    theta = math.atan(opposite / adjacent)

    # calculate the angle due to the taper of the wing to enable matching of
    # the masks to the leading and trailing edges of the wings accounting for
    # the taper
    # alpha = math.acos(self.taper)
    adjacent = wing_length
    opposite = (root_chord
                * WINGTHICKNESSRATIO - wing_taper
                * root_chord * WINGTHICKNESSRATIO) / 2
    alpha = math.atan(opposite / adjacent)

    # assign colour with transparency to the main wing
    mainwing_base = scad.color(
            **yasimtoscad.item_color[wing_type]
        )(aerofoil_base)

    wing_elements = []
    # Use a cylinder to create the wing then apply transforms to:
    #   make basic form
    #   apply sweep
    #   apply dihedral
    #   mirror to right wing

    for wing_element in wing_data.wing_elements:
        if wing_element.wing_element_type in {
                "flap0", "flap1", "spoiler", "slat"}:
            start = wing_element.start
            end = wing_element.end

            if wing_element.wing_element_type in {'flap', 'flap0', 'flap1'}:
                flap_mask = scad.cylinder(
                    d=root_chord * FLAPRATIO,
                    h=wing_length * (end - start),
                    segments=36)
                # flaps are mounted at the rear of the wing, so align the mask
                # with that
                offset = np.zeros((3))
                offset[0] = -root_chord / 2  # align with the TE of the wing
                offset[1] = 0
                offset[2] = wing_length * start  # mask to start of component
                flap_mask = scad.translate(offset)(flap_mask)
                # slant flap to match the taper of the wing
                # shear x along z by sin(theta)
                # do not scale to maintain the length to match the length of
                # the surface
                flap_mask = scad.multmatrix(m=(
                    (1, 0, math.sin(theta), 0),
                    (0, 1,               0, 0),
                    (0, 0,               1, 0),
                    (0, 0,               0, 1),
                    ))(flap_mask)
                flap = scad.intersection()(flap_mask, flap_base)
                i = flap
            elif wing_element.wing_element_type in {'slat'}:
                slat_mask = scad.cylinder(
                    d=root_chord * SLATRATIO,
                    h=wing_length * (end - start),
                    segments=36)

                # slats are mounted at the front of the wing, so align the
                # mask with that
                offset = np.zeros((3))
                offset[0] = root_chord / 2  # align with the LE of the wing
                offset[1] = 0
                offset[2] = wing_length * start  # move mask to start
                slat_mask = scad.translate(offset)(slat_mask)
                # slant slat to match the taper of the wing
                # shear x along z by sin(theta)
                # do not scale to maintain the length to match the length of
                # the surface
                slat_mask = scad.multmatrix(m=(
                    (1, 0, -math.sin(theta), 0),
                    (0, 1,                0, 0),
                    (0, 0,                1, 0),
                    (0, 0,                0, 1),
                    ))(slat_mask)
                slat = scad.intersection()(slat_mask, slat_base)
                i = slat
            elif wing_element.wing_element_type in {'spoiler'}:
                spoiler_mask = scad.cylinder(
                    d=root_chord * SPOILERRATIO,
                    h=wing_length * (end - start),
                    segments=36)
                spoiler_mask = scad.scale(
                        [1, SPOILERTHICKNESSRATIO, 1]
                    )(spoiler_mask)
                # put the spoiler somewhere in the middle of the wing
                offset = np.zeros((3))
                offset[0] = 0
                # align with wing upper surface
                offset[1] = -root_chord * WINGTHICKNESSRATIO / 2
                offset[2] = wing_length * start  # move mask to start
                spoiler_mask = scad.translate(offset)(spoiler_mask)

                # slant spoiler to match the taper of the wing
                # shear y along z by sin(alpha)
                # do not scale to maintain the length to match the length of
                # the surface
                spoiler_mask = scad.multmatrix(m=(
                    (1, 0,               0, 0),
                    (0, 1, math.sin(alpha), 0),
                    (0, 0,               1, 0),
                    (0, 0,               0, 1),
                    ))(spoiler_mask)

                spoiler = scad.intersection()(spoiler_mask, spoiler_base)
                i = spoiler

            wing_elements.append(scad.color(
                    **yasimtoscad.item_color[wing_element.wing_element_type]
                )(i))

    wing_elements.append(mainwing_base)

    for wing_element in wing_elements:
        # align OpenSCAD cylinder with left wing in YASim coordinate system

        # Apply sweep to the vertical wing element before aligning with YASim
        # wing orientation
        sweep_angle = DEG2RAD * wing_data.sweep
        # shear x along z by -sin(sweep angle)
        # scale z by cos(sweep angle) to maintain wing length
        wing_element = scad.multmatrix(m=(
            (1, 0, -math.sin(sweep_angle), 0),
            (0, 1,                      0, 0),
            (0, 0,  math.cos(sweep_angle), 0),
            (0, 0,                      0, 1),
            )
        )(wing_element)

        wing_element_left = scad.rotate([-90, 0, 0])(wing_element)

        wing_element_left = scad.rotate(
            [wing_data.dihedral, 0, 0]
            )(wing_element_left)

        # move the start of the wing element to the root of the wing
        wing_element_left = scad.translate(wing_data.root)(wing_element_left)

        # vstabs are not mirrored, which is set in the wing_data
        if wing_data.mirrored:
            wing = wing_element_left
            wing += scad.mirror([0, 1, 0])(wing_element_left)
        else:
            wing = wing_element_left

        wing_object += wing

    return wing_object

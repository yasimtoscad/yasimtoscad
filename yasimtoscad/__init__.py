import pathlib

import argparse
import xml.etree.ElementTree as ET
import urllib.request
import urllib.parse
from zipfile import ZipFile

import solid as scad

import yasimtoscad.parse as parse
import yasimtoscad.build_scad as build_scad


item_color = {
    'fuselage':        {'c': "blue",          'alpha': 0.01},
    'wing':            {'c': "green",         'alpha': 0.1},
    'hstab':           {'c': "green",         'alpha': 0.1},
    'mstab':           {'c': "green",         'alpha': 0.1},
    'vstab':           {'c': "yellow",        'alpha': 0.1},
    'flap':            {'c': "red",           'alpha': 1.0},
    'flap0':           {'c': "red",           'alpha': 1.0},
    'flap1':           {'c': "red",           'alpha': 1.0},
    'slat':            {'c': "red",           'alpha': 1.0},
    'spoiler':         {'c': "red",           'alpha': 1.0},
    'propeller':       {'c': "blue",          'alpha': 0.01},
    'rotor':           {'c': "blue",          'alpha': 0.01},
    'tank':            {'c': "yellow",        'alpha': 0.5},
    'ballast':         {'c': "magenta",       'alpha': 0.5},
    'weight':          {'c': "black",         'alpha': 1.0},
    'gear':            {'c': "black",         'alpha': 0.5},
    'cockpit':         {'c': "black",         'alpha': 0.5},
    'jet':             {'c': "yellow",        'alpha': 0.5},
    'thruster':        {'c': "red",           'alpha': 0.5},
    'exhaust':         {'c': "red",           'alpha': 0.5},
    'rotated_exhaust': {'c': "red",           'alpha': 0.1},
}

default_elements = {
            "fuselage", "cockpit",  "gear",
            "wing", "hstab", "mstab", "vstab",
            "tank", "ballast", "weight",
            "jet", "thruster",
            "propeller", "rotor",
        }


def convert_yasim_to_scad(yasim_xml: ET.Element, elements: set) -> str:
    '''Converst a YASim flight model to an OpenSCAD file.

    Parses the YASim xml and produces a series of OpenSCAD objects providing
    a visual representation of the flight model definition.

    Parmeters
    ---------
    yasim_xml : xml.etree.ElementTree.Element
        XML containing YASim flight model definitions
    elements : list
        List of YASim flight model elements to include in the OpenSCAD view.

    Returns
    -------
    scad_commands : str
        String ocntaining OpenSCAD commands.

    '''

    scene = scad.union()

    if elements:
        elements_to_process = elements
    else:
        elements_to_process = default_elements

    for item in yasim_xml:
        if item.tag in elements_to_process:
            if item.tag in {"fuselage"}:
                fuselage_data = parse.fuselage(item)
                fuse_mesh = build_scad.fuselage(fuselage_data)
                scene += fuse_mesh
            elif item.tag in {"wing", "hstab", "mstab", "vstab"}:
                wing_data = parse.wing(item)
                wing_mesh = build_scad.wing(wing_data)
                scene += wing_mesh
            elif item.tag in {"propeller"}:
                propeller_data = parse.propeller(item)
                propeller_mesh = build_scad.propeller(propeller_data)
                scene += propeller_mesh
            elif item.tag in {"tank"}:
                tank_data = parse.tank(item)
                tank_mesh = build_scad.tank(tank_data)
                scene += tank_mesh
            elif item.tag in {"ballast"}:
                ballast_data = parse.ballast(item)
                ballast_mesh = build_scad.ballast(ballast_data)
                scene += ballast_mesh
            elif item.tag in {"gear"}:
                gear_data = parse.gear(item)
                gear_mesh = build_scad.gear(gear_data)
                scene += gear_mesh
            elif item.tag in {"cockpit"}:
                cockpit_data = parse.cockpit(item)
                cockpit_mesh = build_scad.cockpit(cockpit_data)
                scene += cockpit_mesh
            elif item.tag in {"jet"}:
                jet_data = parse.jet(item)
                jet_mesh = build_scad.jet(jet_data)
                scene += jet_mesh
            elif item.tag in {"thruster"}:
                thruster_data = parse.thruster(item)
                thruster_mesh = build_scad.thruster(thruster_data)
                scene += thruster_mesh
            elif item.tag in {"rotor"}:
                rotor_data = parse.rotor(item)
                rotor_mesh = build_scad.rotor(rotor_data)
                scene += rotor_mesh
            elif item.tag in {"weight"}:
                weight_data = parse.weight(item)
                weight_mesh = build_scad.weight(weight_data)
                if weight_mesh:
                    scene += weight_mesh
            else:
                print(f"Unhandled element:{item.tag}")

    scene = scad.scale(1000)(scene)

    scad_out = scad.scad_render(scene)
    return scad_out


def get_yasim_files(flightgear_dir: pathlib.Path) -> list:
    '''Extracts a list of YASim files in a FlightGear model directory.

    Examines the FlightGear -set.xml file and searches for YASim flight model
    definitions.

    Parmeters
    ---------
    flightgear_dir : pathlib.Path
        Directory containing a flightgear model.
    output_dir : pathlib.Path
        Directory to store the OpenSCAD views, defaults to current directory.
    elements : list
        List of YASim flight model elements to include in the OpenSCAD view.

    Returns
    -------
    yasim_file_list : list
        List of YASim files within the FlightGear directory (with path).

    '''

    file_list = (entry
                 for entry
                 in flightgear_dir.iterdir()
                 if entry.is_file())

    model_list = []
    for file in file_list:
        if file.name.endswith('-set.xml'):
            model_list.append(file)

    yasim_files = []
    for file in model_list:
        set_file = ET.parse(file)
        xml_root = set_file.getroot()
        for sim in xml_root.findall("sim"):
            flight_model = sim.find("flight-model")

            if flight_model is not None and flight_model.text == "yasim":
                aero = sim.find("aero")
                yasim_base = aero.text
                file_path = pathlib.Path(file.parent / f"{yasim_base}.xml")
                yasim_files.append(file_path)

    return yasim_files


def process_flightgear_dir(
        flightgear_dir: pathlib.Path,
        output_dir:     pathlib.Path,
        elements:       list[str]) -> None:
    '''Produces OpenSCAD views from YASim model definitions from a FlightGear
    model directory.

    Parmeters
    ---------
    flightgear_dir : pathlib.Path
        Directory containing a flightgear model.
    output_dir : pathlib.Path
        Directory to store the OpenSCAD views, defaults to current directory
    elements : list
        List of YASim flight model elements to include in the OpenSCAD view
    Returns
    -------
        None
    '''
    yasim_model_dir_name = pathlib.Path(flightgear_dir.stem)

    if output_dir is not None:
        output_dir = pathlib.Path(output_dir / yasim_model_dir_name)
    else:
        output_dir = yasim_model_dir_name

    if not output_dir.is_dir():
        output_dir.mkdir()

    yasim_file_list = get_yasim_files(flightgear_dir)

    for yasim_file in yasim_file_list:

        scad_file = yasim_file.with_suffix('.scad').name

        process_yasim_file(
            yasim_file=yasim_file,
            scad_file=scad_file,
            output_dir=output_dir,
            elements=elements)


def process_yasim_file(
        yasim_file: pathlib.Path,
        scad_file:  pathlib.Path,
        output_dir: pathlib.Path,
        elements:   list[str]) -> None:
    '''Produces OpenSCAD views from a YASim model definition and writes them to
    a file.

    Parmeters
    ---------
    yasim_file : pathlib.Path
        File containing a YASim model definition.
    scad_file : pathlib.Path
        File where OpenSCAD view is written.
    output_dir : pathlib.Path
        Directory to store the OpenSCAD views, defaults to current directory
    elements : list
        List of YASim flight model elements to include in the OpenSCAD view
    Returns
    -------
        None
    '''

    with open(yasim_file, mode='r', encoding='utf-8') as f_yasim:
        yasim_data = f_yasim.read()
        yasim_tree = ET.fromstring(yasim_data)

        scad_out = convert_yasim_to_scad(yasim_tree, elements)

        if output_dir is not None:
            scad_file = pathlib.Path(output_dir / scad_file)

        with open(scad_file, mode="w") as f_scad:
            f_scad.write(scad_out)


def process_remote_model(
        remote_model_url:  str,
        flightgear_dir:    pathlib.Path,
        zip_cache_dir:     pathlib.Path,
        output_dir:        pathlib.Path,
        elements:          list[str]) -> None:
    '''Produces OpenSCAD views from YASim model definitions from a remote
    FlightGear model file.

    Parmeters
    ---------
    remote_model_url : str
        Url of remote model zip file
    zip_cache_dir : pathlib.Path
        Local directory for storing retrieved model zip files.
    flightgear_dir : pathlib.Path
        Directory containing a flightgear model.
    output_dir : pathlib.Path
        Directory to store the OpenSCAD views, defaults to current directory
    elements : list
        List of YASim flight model elements to include in the OpenSCAD view
    Returns
    -------
        None
    '''
    # get the file name and retrieve the file from the url and put it in zipdir
    if zip_cache_dir is not None:
        filename = pathlib.Path(
            zip_cache_dir / remote_model_url.split('/')[-1])
    else:
        filename = pathlib.Path(remote_model_url.split('/')[-1])

    if not filename.is_file():
        urllib.request.urlretrieve(remote_model_url, filename)

    # once the file has been retrieved we can extract the flightgear model
    # into flightgear_dir
    if flightgear_dir is not None:
        model_dir = pathlib.Path(flightgear_dir / filename.stem)
    else:
        model_dir = pathlib.Path(filename.stem)

    if not model_dir.is_dir():
        with ZipFile(filename, 'r') as model_zip:
            model_zip.extractall(path=flightgear_dir)

    process_flightgear_dir(
        flightgear_dir=model_dir,
        output_dir=output_dir,
        elements=elements)


def process_remote_model_list(
        remote_model_list: pathlib.Path,
        flightgear_dir:    pathlib.Path,
        zip_cache_dir:     pathlib.Path,
        output_dir:        pathlib.Path,
        elements:          list[str]) -> None:
    '''Produces OpenSCAD views from multiple YASim model definitions from remote
    FlightGear model files.

    Parmeters
    ---------
    remote_model_list : pathlib.Path
        File containing a ist of remote model zip files.
    zip_cache_dir : pathlib.Path
        Local directory for storing retrieved model zip files.
    flightgear_dir : pathlib.Path
        Directory containing a flightgear model.
    output_dir : pathlib.Path
        Directory to store the OpenSCAD views, defaults to current directory
    elements : list
        List of YASim flight model elements to include in the OpenSCAD view
    Returns
    -------
        None
    '''

    # Get a list of urls from where the models should be retrieved
    model_list = []
    with open(remote_model_list) as f:
        for line in f.readlines():
            model_list.append(line.rstrip())

    for remote_model_url in model_list:
        process_remote_model(
            remote_model_url=remote_model_url,
            flightgear_dir=flightgear_dir,
            zip_cache_dir=zip_cache_dir,
            output_dir=output_dir,
            elements=elements)


def build_parser() -> argparse.ArgumentParser:
    '''Creates a parser based on the required argument structure

    Parmeters
    ---------
    None

    Returns
    -------
    argparse.ArgumentParser
        Namespace derived from the parsed parameters.
    '''
    parser = argparse.ArgumentParser(description='YASim to OpenSCAD converter')

    subparsers = parser.add_subparsers(
        title='actions',
        dest='action',
        required=True)

    sp_yasim_file = subparsers.add_parser(
        'yasim-file',
        description='OpenSCAD file from a YASim file',
        help='produce OpenSCAD files for a YASim file')
    sp_fgdir = subparsers.add_parser(
        'flightgear-dir',
        description='OpenSCAD files from FlightGear model',
        help='produce OpenSCAD files for all YASim files'
        'in a FlightGear aircraft directory')
    sp_retrieve_model = subparsers.add_parser(
        'remote-model-url',
        description='OpenSCAD files from a remote FlightGear model',
        help='produce OpenSCAD files for all YASim files'
        'in a FlightGear model retrieved from a remote repository')
    sp_retrieve_from_list = subparsers.add_parser(
        'remote-model-list',
        description='OpenSCAD files from a list of FlightGear models',
        help='produce OpenSCAD files for all YASim files'
        'from a list of FlightGear models retrieved from a remote repository')

    for sp in {sp_retrieve_from_list, sp_retrieve_model,
               sp_fgdir, sp_yasim_file}:
        if sp is sp_retrieve_from_list:
            sp.add_argument(
                'remote_model_list',
                metavar='remote-model-list', type=str,
                help='file containing urls with location of FlightGear models')
        elif sp is sp_retrieve_model:
            sp.add_argument(
                'remote_model_url',
                metavar='remote-model-url', type=str,
                help='url with location of FlightGear model')
        elif sp is sp_yasim_file:
            sp.add_argument(
                'yasim_file',
                metavar='yasim-file', type=str,
                help='file containing YASim model definition')
            sp.add_argument(
                'scad_file',
                metavar='scad-file', type=str, nargs='?',
                help='file containing generated OpenSCAD commands')

        if sp in {sp_retrieve_from_list, sp_retrieve_model, sp_fgdir}:
            sp.add_argument(
                '--zip-cache-dir', type=str,
                help='directory to contain retrieved'
                'zipped FlightGear model definitions')
            sp.add_argument(
                '--flightgear-dir', type=str,
                help='directory to contain unzipped'
                'FlightGear model definitions')

        sp.add_argument(
            '--output-dir', type=str,
            help='directory to write OpenSCAD files')
        sp.add_argument(
            '--elements', type=str, nargs='+',
            help='YASim elements to convert to OpenSCAD model')

    return parser


def build_command_parameters(args: argparse.Namespace) -> dict:
    '''Parse arguments into action and parameters

    Parmeters
    ---------
    args : arparse.Namespace
        Namespace derived from the parsed parameters.

    Returns
    -------
    dict
        Dictionary containing an action, with associated parameters determined
        from the arguments
    '''

    elements = args.elements

    if args.output_dir is not None:
        output_dir = pathlib.Path(args.output_dir)
    else:
        output_dir = None

    if args.action == "yasim-file":
        yasim_file = pathlib.Path(args.yasim_file)
        if args.scad_file is not None:
            scad_file = pathlib.Path(args.scad_file)
        else:
            # use yasm file name as filename and .scad as extension
            scad_file = pathlib.Path(yasim_file.with_suffix('.scad').name)

        command = {
            'action': args.action,
            'yasim_file_arguments': {
                'yasim_file': yasim_file,
                'scad_file':  scad_file,
                'output_dir': output_dir,
                'elements':   elements}
            }
    else:
        if args.flightgear_dir is not None:
            flightgear_dir = pathlib.Path(args.flightgear_dir)
        else:
            flightgear_dir = None

        if args.action == "flightgear-dir":
            command = {
                'action': args.action,
                'flightgear_dir_arguments': {
                    'flightgear_dir': flightgear_dir,
                    'output_dir': output_dir,
                    'elements':   elements}
                }
        else:
            if args.zip_cache_dir is not None:
                zip_cache_dir = pathlib.Path(args.zip_cache_dir)
            else:
                zip_cache_dir = None

            if args.action == "remote-model-list":
                remote_model_list = pathlib.Path(args.remote_model_list)
                command = {
                    'action': args.action,
                    'remote_model_list_arguments': {
                        'remote_model_list': remote_model_list,
                        'flightgear_dir':    flightgear_dir,
                        'output_dir':        output_dir,
                        'zip_cache_dir':     zip_cache_dir,
                        'elements':          elements}
                    }
            elif args.action == "remote-model-url":
                remote_model_url = args.remote_model_url

                command = {
                    'action': args.action,
                    'remote_model_url_arguments': {
                        'remote_model_url':  remote_model_url,
                        'flightgear_dir':    flightgear_dir,
                        'output_dir':        output_dir,
                        'zip_cache_dir':     zip_cache_dir,
                        'elements':          elements}
                    }

    return command


def main(argv: list[str]):

    parser = build_parser()

    args = parser.parse_args(argv[1:])

    command = build_command_parameters(args)

    if command['action'] == 'yasim-file':
        process_yasim_file(**command['yasim_file_arguments'])
    elif command['action'] == 'flightgear-dir':
        process_flightgear_dir(**command['flightgear_dir_arguments'])
    elif command['action'] == 'remote-model-list':
        process_remote_model_list(**command['remote_model_list_arguments'])
    elif command['action'] == 'remote-model-url':
        process_remote_model(**command['remote_model_url_arguments'])


if __name__ == "__main__":
    import sys
    main(sys.argv)

# YASim2SCAD

YASim model visualisation to OpenSCAD command converter.
This application provides a way of visualising [YASim elements](http://wiki.flightgear.org/YASim) by converting the YASim flight model to a series of [OpenSCAD](https://www.openscad.org/) commands.

The OpenSCAD file produced can be loaded into a number of utilities for viewing OpenSCAD models, including the OpenSCAD viewer.

YASim is used in the [FlightGear](https://www.flightgear.org/) and some of the options can interpret YASim models within a FlightGear model.

A number of options exist:

- Converting from a single YASim file to a single OpenSCAD file:
```
    yasimtoscad yasim-file <yasim-file> <scad-file>
```
- Converting all YASim files from a FlighGear model directory:
```
    yasimtoscad flightgear-dir <FlightGear model directory>
```
- Convert all YASim files from a remote FlightGear model:
```
    yasimtoscad remote_model_url <url of remote FlightGear model>
```


Some examples:

![Fuoga Magister](doc/img/fouga-yasim.png)
Fouga Magister (YASim), BARANGER Emmanuel (3D/FDM)


![Sikorsky CH53e](doc/img/ch53e.png)
Sikorsky CH-53E Super Stallion (Yasim), Pierre Duval, Josh Babcok, StuartC

Images generated from published FlightGear models:


Copyright [Graham James Addis](graham@addis.org.uk)

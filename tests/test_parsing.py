import os

# from .context import yasimtoscad

from yasimtoscad import parse as yp

import unittest
import xml.etree.ElementTree as ET

print(os.path.dirname(__file__))

KG_PER_LB = 0.453592

fuselage_test_data = [
    {
        'Scenario': 'Fuselage along x',
        'xml': '<fuselage '
                    'ax=" 0.000" ay=" 0.000" az=" 0.000" '
                    'bx=" 1.000" by=" 0.000" bz=" 0.000" '
                    'width="0.100" taper="0.90" midpoint="1"'
                    '/>',
        'parms': {
            'start': {'ax': 0.000, 'ay': 0.000, 'az': 0.000},
            'end':   {'bx': 1.000, 'by': 0.000, 'bz': 0.000},
            'width':    0.1,
            'taper':    0.90,
            'midpoint': 1}
    },
    {
        'Scenario': 'Fuselage along y',
        'xml': '<fuselage '
                    'ax=" 0.000" ay=" 0.000" az=" 0.000" '
                    'bx=" 0.000" by=" 1.000" bz=" 0.000" '
                    'width="0.100" taper="0.90" midpoint="1"'
                    '/>',
        'parms': {
            'start': {'ax': 0.000, 'ay': 0.000, 'az': 0.000},
            'end':   {'bx': 0.000, 'by': 1.000, 'bz': 0.000},
            'width':    0.1,
            'taper':    0.90,
            'midpoint': 1}
    },
    {
        'Scenario': 'Fuselage along z',
        'xml': '<fuselage '
                    'ax=" 0.000" ay=" 0.000" az=" 0.000" '
                    'bx=" 0.000" by=" 1.000" bz=" 0.000" '
                    'width="0.100" taper="0.90" midpoint="1"'
                    '/>',
        'parms': {
            'start': {'ax': 0.000, 'ay': 0.000, 'az': 0.000},
            'end':   {'bx': 0.000, 'by': 0.000, 'bz': 1.000},
            'width':    0.1,
            'taper':    0.90,
            'midpoint': 1}
    },
    {
        'Scenario': 'Fuselage along -x',
        'xml': '<fuselage '
                    'ax=" -1.000" ay=" 0.000" az=" 0.000" '
                    'bx="  0.000" by=" 0.000" bz=" 0.000" '
                    'width="0.100" taper="0.90" midpoint="1"'
                    '/>',
        'parms': {
            'start': {'ax': -1.000, 'ay': 0.000, 'az': 0.000},
            'end':   {'bx':  0.000, 'by': 0.000, 'bz': 0.000},
            'width':    0.1,
            'taper':    0.90,
            'midpoint': 1}
    },
    {
        'Scenario': 'Fuselage along -y',
        'xml': '<fuselage '
                    'ax=" 0.000" ay=" -1.000" az=" 0.000" '
                    'bx=" 0.000" by="  0.000" bz=" 0.000" '
                    'width="0.100" taper="0.90" midpoint="1"'
                    '/>',
        'parms': {
            'start': {'ax': 0.000, 'ay': -1.000, 'az': 0.000},
            'end':   {'bx': 0.000, 'by':  0.000, 'bz': 0.000},
            'width':    0.1,
            'taper':    0.90,
            'midpoint': 1}
    },
    {
        'Scenario': 'Fuselage along -z',
        'xml': '<fuselage '
                    'ax=" 0.000" ay=" 0.000" az=" -1.000" '
                    'bx=" 0.000" by=" 0.000" bz="  0.000" '
                    'width="0.100" taper="0.90" midpoint="1"'
                    '/>',
        'parms': {
            'start': {'ax': 0.000, 'ay': 0.000, 'az': -1.000},
            'end':   {'bx': 0.000, 'by': 0.000, 'bz':  0.000},
            'width':    0.1,
            'taper':    0.90,
            'midpoint': 1}
    },
]

tank_test_data = [
    {
        'Scenario': 'Basic settings x = .1',
        'xml': '<tank x="0.1" y="0" z="0" capacity="90"/>',
        'parms': {
            'origin': {"x": 0.100, "y": 0.000, "z": 0.000},
            'capacity_kg': 90 * KG_PER_LB},
    },
    {
        'Scenario': 'Basic settings y = .1',
        'xml': '<tank x="0" y="0.1" z="0" capacity="90"/>',
        'parms': {
            'origin': {"x": 0.000, "y": 0.100, "z": 0.000},
            'capacity_kg': 90 * KG_PER_LB},
    },
    {
        'Scenario': 'Basic settings z = .1',
        'xml': '<tank x="0" y="0" z="0.1" capacity="90"/>',
        'parms': {
            'origin': {"x": 0.000, "y": 0.000, "z": 0.100},
            'capacity_kg': 90 * KG_PER_LB},
    },
    {
        'Scenario': 'Basic settings negative loc',
        'xml': '<tank x="-0.1" y="-0.1" z="-0.1" capacity="90"/>',
        'parms': {
            'origin': {"x": -0.100, "y": -0.100, "z": -0.100},
            'capacity_kg': 90 * KG_PER_LB},
    },
    {
        'Scenario': 'Basic settings negative capacity-lbs',
        'xml': '<tank x="-0.1" y="-0.1" z="-0.1" capacity-lbs="90"/>',
        'parms': {
            'origin': {"x": -0.100, "y": -0.100, "z": -0.100},
            'capacity_kg': 90 * KG_PER_LB},
    },
    {
        'Scenario': 'Basic settings negative capacity-kg',
        'xml': '<tank x="-0.1" y="-0.1" z="-0.1" capacity-kg="90"/>',
        'parms': {
            'origin': {"x": -0.100, "y": -0.100, "z": -0.100},
            'capacity_kg': 90},
    },
]


class TestParser(unittest.TestCase):

    def test_parse_fuselage(self):
        for fuselage_entry in fuselage_test_data:
            fuselage_xml = fuselage_entry['xml']
            fuselage_result = fuselage_entry['parms']

            with self.subTest(fuselage_entry['Scenario']):
                xml_root = ET.fromstring(fuselage_xml)
                fuselage_data = yp.fuselage(xml_root)
                self.assertEqual(fuselage_result['start']['ax'],
                                 fuselage_data.start[0])
                self.assertEqual(fuselage_result['start']['ay'],
                                 fuselage_data.start[1])
                self.assertEqual(fuselage_result['start']['az'],
                                 fuselage_data.start[2])
                self.assertEqual(fuselage_result['width'],
                                 fuselage_data.width)

    def test_parse_tank(self):
        for tank_entry in tank_test_data:
            tank_xml = tank_entry['xml']
            tank_result = tank_entry['parms']

            with self.subTest(tank_entry['Scenario']):
                xml_root = ET.fromstring(tank_xml)
                tank_data = yp.tank(xml_root)
                self.assertEqual(tank_result['origin']['x'],
                                 tank_data.origin[0])
                self.assertEqual(tank_result['origin']['y'],
                                 tank_data.origin[1])
                self.assertEqual(tank_result['origin']['z'],
                                 tank_data.origin[2])
                self.assertEqual(tank_result['capacity_kg'],
                                 tank_data.capacity_kg)


if __name__ == '__main__':
    unittest.main()
